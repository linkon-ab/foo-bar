package com.silverrailtech.services;

import com.silverrailtech.repositories.ExamplesRepository;
import com.silverrailtech.resources.requests.ExampleCreateRequest;
import com.silverrailtech.resources.requests.ExampleUpdateRequest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Objects;

@RequestScoped
public class ExamplesService
{

    @Inject
    private ExamplesRepository examplesRepository;

    public Object getList(String filter)
    {
        if(Objects.isNull(filter))
        {
            // return full list
            return new ArrayList<>();
        }
        else
        {
            //return filtered list
            return new ArrayList<>();
        }
    }

    public Object find(String id)
    {
        return new Object();
    }

    public Object createOrUpdate(ExampleUpdateRequest exampleUpdateRequest)
    {
        return null;
    }

    public Object create(ExampleCreateRequest exampleCreateRequest)
    {
        return null;
    }

    public Object delete(String id)
    {
        return null;
    }
}
