package com.silverrailtech.repositories;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RequestScoped
public class ExamplesRepository
{
    @PersistenceContext(unitName = "primary")
    private EntityManager em;
}
