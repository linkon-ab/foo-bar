package com.silverrailtech;

import com.silverrailtech.common.exceptions.handlers.DefaultExceptionHandler;
import com.silverrailtech.common.filters.ApiOriginFilter;
import com.silverrailtech.common.filters.AuthorizationFilter;
import com.silverrailtech.resources.ExamplesResourceV1;
import io.swagger.jaxrs.config.BeanConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

import static java.lang.System.getenv;
import static java.util.Objects.requireNonNull;

@ApplicationPath("")
public class FooBarApplication extends Application
{

    final HashSet<Object> singletons = new HashSet<>();
    final BeanConfig beanConfig;

    public FooBarApplication()
    {
        beanConfig = new BeanConfig();
        beanConfig.setTitle("Title of the API");
        beanConfig.setDescription("Description of the API");
        beanConfig.setContact("Contact");
        beanConfig.setVersion("1.0");
        beanConfig.setSchemes(new String[] { "http" });
        beanConfig.setHost(requireNonNull(getenv("SWAGGER_HOST"),
                "SWAGGER_HOST not set in environment!"));
        beanConfig.setBasePath("/foo/bar");
        beanConfig.setResourcePackage("com.silverrailtech.resources");
        beanConfig.setScan(false);
    }

    @Override
    public Set<Class<?>> getClasses()
    {
        final HashSet<Class<?>> set = new HashSet<>();

        set.add(ExamplesResourceV1.class);
        
        set.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        set.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

        set.add(DefaultExceptionHandler.class);
        set.add(AuthorizationFilter.class);
        set.add(ApiOriginFilter.class);

        return set;
    }

    @Override
    public Set<Object> getSingletons()
    {
        return singletons;
    }

}
