package com.silverrailtech.resources;

import com.silverrailtech.resources.requests.ExampleCreateRequest;
import com.silverrailtech.resources.requests.ExampleUpdateRequest;
import com.silverrailtech.services.ExamplesService;
import io.swagger.annotations.Api;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.silverrailtech.common.response.ResponseFactory.ok;

// Uncomment row below to enable AUTH-TOKEN checking
// @AuthorizationFilter.Secured
@Api(tags = { "examples" })
@Path("v1/examples")
@Produces(MediaType.APPLICATION_JSON)
public class ExamplesResourceV1
{

    @Inject
    private ExamplesService examplesService;

    /**
     * Example of endpoint that returns a list of resources
     */
    @GET
    public Response exampleGetList(@QueryParam("filter") String filter)
    {
        return ok(examplesService.getList(filter));
    }

    /**
     * Example of endpoint that returns a single resource
     */
    @GET
    @Path("/{id}")
    public Response exampleGetSingle(@PathParam("id") String id)
    {
        return ok(examplesService.find(id));
    }

    /**
     * Example of endpoint that updates or creates a resource
     */
    @PUT
    @Path("/{id}")
    public Response examplePut(@PathParam("id") String id, @Valid ExampleUpdateRequest exampleUpdateRequest)
    {
        /*
         * TODO: return 201 if updated and 200 if created
         */
        return ok(examplesService.createOrUpdate(exampleUpdateRequest));
    }

    /**
     * Example of endpoint that creates a resource
     */
    @POST
    public Response examplePost(@Valid ExampleCreateRequest exampleCreateRequest)
    {
        /*
         * TODO: return created object?
         */
        return ok(examplesService.create(exampleCreateRequest));
    }

    /**
     * Example of endpoint that deletes a resource
     */
    @DELETE
    @Path("/{id}")
    public Response exampleDelete(@PathParam("id") String id)
    {
        /*
         * TODO: number of deletes?
         */
        return ok(examplesService.delete(id));
    }

}
