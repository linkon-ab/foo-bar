package com.silverrailtech.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

//@Entity
//@Table(name = "EXAMPLE_ENTITY", schema="DOMAIN")
public class ExampleEntity
{
    //@Column(name="VALUE1")
    private String value1;

    //@Column(name="VALUE2")
    private BigDecimal value2;

    //@Column(name="VALUE3")
    private LocalDate value3;

    //@Column(name="VALUE4")
    private LocalTime value4;

    public ExampleEntity(String value1, BigDecimal value2, LocalDate value3, LocalTime value4)
    {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    public ExampleEntity()
    {
    }

    public String getValue1()
    {
        return value1;
    }

    public void setValue1(String value1)
    {
        this.value1 = value1;
    }

    public BigDecimal getValue2()
    {
        return value2;
    }

    public void setValue2(BigDecimal value2)
    {
        this.value2 = value2;
    }

    public LocalDate getValue3()
    {
        return value3;
    }

    public void setValue3(LocalDate value3)
    {
        this.value3 = value3;
    }

    public LocalTime getValue4()
    {
        return value4;
    }

    public void setValue4(LocalTime value4)
    {
        this.value4 = value4;
    }
}
