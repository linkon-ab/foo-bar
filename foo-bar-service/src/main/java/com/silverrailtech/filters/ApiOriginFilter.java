package com.silverrailtech.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class ApiOriginFilter implements javax.servlet.Filter
{

    private final static Logger log = LoggerFactory.getLogger(ApiOriginFilter.class);

    @Resource(lookup = "java:/PetraDS")
    private DataSource dataSource;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        log.info("doFilter");

        HttpServletResponse res = (HttpServletResponse) response;
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        res.addHeader("Access-Control-Allow-Headers", "Content-Type");

        //TODO: refactor environment extraction code
        try (final Connection c = dataSource.getConnection())
        {
            final String url = c.getMetaData().getURL();
            res.addHeader("Environment", url.substring(url.length() - 1).toUpperCase());
        }
        catch (SQLException e)
        {
            res.addHeader("Environment", "None");
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {
        log.info("destroy");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        log.info("init");
    }

}
