#!/usr/bin/env bash

image=foo-bar

docker build --tag=$image:0.0.$BUILD_NUMBER .

docker tag $image:0.0.$BUILD_NUMBER alp-lnk-131.linkon.se:5000/$image:0.0.$BUILD_NUMBER

docker push alp-lnk-131.linkon.se:5000/$image:0.0.$BUILD_NUMBER