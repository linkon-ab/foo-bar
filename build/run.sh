#!/usr/bin/env bash

env=b
image=foo-bar-service

docker build --tag=$image .
docker stop $image"-"$env || true && docker rm $image"-"$env || true
docker run -d -it -p 9123:8080 --name $image"-"$env -e DEPLOY_ENV=$env $image:latest
