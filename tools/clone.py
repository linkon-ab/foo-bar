import sys
import os
import shutil

DOC = """

Description:

    Clone the directories and files from rest-template project to a new REST project 
    and make name replaces for domain and subdomain in directories and files.
    
Usage: 

    python clone.py   source-dir  target-dir  domain  subdomain  port
    
    source-dir      The directory in which this template project is found
    
    target-dir      A new empty directory where the cloned project is to be placed.
                    This directory must NOT be a subdirectory of source-dir.
    
    domain          The name of the REST domain
    
    subdomain       The name of the REST domain

    port            The port that the application should be deployed on    

"""



def rename_dirs(source_root, domain, subdomain):
    while rename_dir(source_root, domain, subdomain):
        pass

                    
def rename_dir(source_root, domain, subdomain):
    for root, dirs, files in os.walk(source_root):
        for file in files:
            if 'DomainSubDomain' in file:
                new_file = file.replace('DomainSubDomain', '%s%s%s%s' % (domain[0].upper(), domain[1:].lower() , subdomain[0].upper(), subdomain[1:].lower()))
                print('File change %s => %s' % (os.path.join(root, file), os.path.join(root, new_file)))
                shutil.move(os.path.join(root, file), os.path.join(root, new_file))
                return True
        for dir in dirs:
            if 'domain-subdomain' in dir:
                new_dir = dir.replace('domain-subdomain', '%s-%s' % (domain, subdomain))
                print('Dir change %s => %s' % (os.path.join(root, dir), os.path.join(root, new_dir)))
                shutil.move(os.path.join(root, dir), os.path.join(root, new_dir))
                return True
    return False


def replace_file_contents(source_root, domain, subdomain, port):
    for root, dirs, files in os.walk(source_root):
        for file in [f for f in files if f.endswith('.xml') or f.endswith('.java') or f == 'index.html' or f == 'Dockerfile' or f.endswith('.sh')]:
            replace_file_content(root, file, domain, subdomain, port)


def replace_file_content(source_root, file, domain, subdomain, port):
    domain = domain.lower()
    subdomain = subdomain.lower()
    path = os.path.join(source_root, file)
    with open(path, 'r') as f:
        text = f.read()
    with open(path, 'w') as f:
        text = text.replace('domain-subdomain', '%s-%s' % (domain, subdomain))
        text = text.replace('DomainSubDomain', '%s%s%s%s' % (domain[0].upper(), domain[1:], subdomain[0].upper(), subdomain[1:]))
        text = text.replace('domain/subdomain', '%s/%s' % (domain, subdomain))
        text = text.replace('#PORT', '%s' % port)
        f.write(text)
    print('Text replaced in: %s' % path)
        

def ignore(path, listdirs):
    """
    Don't clone the git directory
    """
    if '.git' in listdirs:
        return ['.git']
    else:
        return []
    

def clone(source_dir, target_dir, domain, subdomain, port):    
    shutil.copytree(source_dir, target_dir, ignore=ignore)
    rename_dirs(target_dir, domain, subdomain)
    replace_file_contents(target_dir, domain, subdomain, port)
    
    
def main():        
    if '-h' in sys.argv:
        print(DOC)
    elif len(sys.argv) != 6:
        print("Wrong number of arguments=%d!!! " % len(sys.argv), sys.argv[0]);
        print(DOC)
    else:    
        _, source_dir, target_dir, domain, subdomain, port = sys.argv
        if os.path.exists(target_dir):
            print("Error: Target directory alreday exists!")
        else:
            clone(source_dir, target_dir, domain, subdomain, port)


if __name__ == "__main__":
    main()    