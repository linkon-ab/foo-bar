![Silverrail](../silverraillogo.png)

# REST Developers Guide

## Introduction
This document explains how to build a REST service using the  REST template project.
 

## General Guidelines and Naming conventions
Genereal guidelines for developing REST Services can be found here...  
[https://confluence-linkon.riada.se/display/COEDEV/Naming+of+REST+Services](https://confluence-linkon.riada.se/display/COEDEV/Naming+of+REST+Services)

The naming strategy for REST Services is explained here...  
[https://confluence-linkon.riada.se/display/COEDEV/Naming+of+REST+Services](https://confluence-linkon.riada.se/display/COEDEV/Naming+of+REST+Services)

## The template project
The template project is found in the BitBucket repository
[https://bitbucket.org/linkon-ab/rest-template
](https://bitbucket.org/linkon-ab/rest-template)

The purpose of the template project is to make it easy to create a new REST service project, and to get the same main structure in all REST services.

To create a new REST project you just clone the template project and makes some renamings as described below. 

### Use clone.py
One way of doing a clone is to use the Python script clone.py, that is to be found in the tools directory of the REST-template project.
Of course you must have Python installed on your machine, for this to work!

I order to make a clone, 

1. open a cmd-window 
2. change directory to the tools directory
3. execute the the script with: `python clone.py ..  new-target-directory  my-domain-name  my-subdomain-name`
4. go to the newly created directory new-target-directory
5. execute the command: `mvn clean install`

Now you hav a working skeleton for a REST-service.

## Development Environment

### Prerequisites
We assume that you now have a REST-service commited in a repository and that you have cloned the repositor to a local directory on your host.

### Wildfly server
In order to be able to test the REST server on your local machine you have to have a running a Wilfly Server.
[Download](http://wildfly.org/downloads/ "Download") the version to be used and unzip oa a suitable directory on your computer. 

Make sure the JAVA_HOME environment variable points to a Java version matching the server version. If you don't want to change the global JAVA_HOME variable you can create a command-file that starts the server, like this:

    set JAVA_HOME=%ProgramFiles%\Java\jdk-11.0.1
	start standalone.bat

You need to add a datasource to the server in order to be able to access a database. Start the server by executing the standalone.bat command. On the admin console http://localhost:9990

*  Deploy the jar files for the DB driver
   In the Deployments tab, click Add-button and select 'Upload a new deployment' and then select the jar files for the driver. The jar files can be found in any REST-service under directory Build/Ibm (For DB2 it's 2 jar files).
*  Create a Datasource
   In the Configuration tab, select Subsystems -> Datasources -> Non-XA, click Add-button.
   The JNDI name of the Datasource shall be the same as the setting in the percistence.xml file in the REST-project (***java:/PetraDS***). Let Wildfly detect driver (special tab). Connection URL (to B environment) jdbc:db2://db2luw_f_test.linkon.se:60070/DPETRAB. Use a user account that you know works for the database (like dpetchma).

You also need to set some environment variables on your machine.

-  SWAGGER_HOST=localhost:8080
-  DEV_TOKEN_CHECK_URL=http://kronos.linkon.se/auth/roles/v1/login
  

Now you bild your REST-service (mvn clean install) and copy the resulting *.war file (found in the target directoey of the REST-project) to the standalone/deployments directory of the Wildfly server.

### IDE
Depending on which IDE that is used, different settings for Maven etc. may be needed.

[Intellij Configuration](#IntelliJConfig).

## Organization of Code
It's easier to understand the architecture of REST services if the same pattern is followed. This section describes the common pattern that is also implemented in the REST-template project.

The application code is found in the *domain-subdomain-service/src/main/java/com/silverrailtech* directory.

This directory contains five subdirectories.


- filters
- models
- repositories
- resources
- services

Content explained below.  

#### Application object
The application class needed py RestEasy is found in the file DomainSubDomainApplication.java.

#### Resources (resources)
All REST-resources (defining end-points) with response- and request-definitions are found in the *resources* directory.

#### Services (services)
Resources are using Service-object for business logic. The code for the Services are found in the *services* directory.

#### Data Access Code (repository)
All code retrieving and saving data in a database are found in the *repositories* directory.

#### Data Models (models)
Data models used in the application are found in the *models* directory.

#### Filters (filters)
Filters are moved to a service common to all REST-services -> com.silverrailtech.common.filters.AuthorizationFilter;
(Filters are used to check incoming- and outgoing data, to an from a JAX-RS resource.
Filter classes are found in the *filters* directory.)


## Deployment

### Test Environment (B)
This part is specific for team Kronos but the pattern can be followed for any team. The idea is that every team has it's on server for REST services that is the B test environment.

When a REST service is changed and the changes committed to the VCS, a Jenkins job is started that builds the war artifact for the service, creates a docker container and deploys the container to the B environment server (kronos.linkon.se).

The Jenkins jobs runs on kronos.linkon.se at port 8080. The jobs are named [domain]-[subtomain]-service. 
> Example: ordr-currencies-test

### Release to Test Environments (Q, R, A)
When it's time to relase a version to other test environments than B, another Jenkins jub is run manually. The name of this job is:

> [domain]-[subdomain].service-release

 
A release docker container including the REST service is now created.
This container is deployed to different environments with other Jenkins jobs found at

> http://elastic-stack.linkon.local:7080

Here you can find Jenkins jobs to deploy the docker container to REST server machines in all test environments.

### Release to Production
Handled by OP on request.

# Appendix

## IntelliJ Configuration 

Make sure you have the correct Maven settings (File -> Settings -> Build, Execution and Deployment -> Build Tool -> Maven)
  
Create a Run configuration that builds and deploys your project on the local Wildfly server.

Run -> Edit Configurations
Select Defaults.
Select JBoss Server -> Local

-   Server -> Configure -> JBoss Homme
-   Deployment -> + -> Artifact -> (Select artifact) 
-   Startup Connection -> Environment variables -> 
	-   SWAGGER_HOST = localhost:8080
	-   DEV_TOKEN_CHECK_URL = http:kronos.linkon.se/auth/roles/v1/login

Copy the project's swagger directory to the welcome-content directory of your local Wildfly installation. Rename the directory swagger-domain-subdomin. This action makes localhost:8080/swagger-*domain-subdomain*, work.

Use the Silverrailtech style guide for Java code. Select File -> Settings -> Editor -> Code Style -> Java, and import the CheckStyle.xml that can be found at [https://subversion:8443/svn/Petra2SOA/Utvecklingsmiljö/CheckStyle/trunk/](https://subversion:8443/svn/Petra2SOA/Utvecklingsmilj%C3%B6/CheckStyle/trunk/ "https://subversion:8443/svn/Petra2SOA/Utvecklingsmilj%C3%B6/CheckStyle/trunk/")


